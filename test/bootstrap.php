<?php

/**
 * This file is part of apk/file-iterators
 *
 * (c) Copyright 2015 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

require_once __DIR__ . '/../vendor/autoload.php';
