<?php

/**
 * This file is part of apk/file-iterators
 *
 * (c) Copyright 2015 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace FileIteratorsTests\Functional;

use Apk\FileIterators\Generator\CsvFileReader;
use Apk\FileIterators\Consumer\CsvFileWriter;

class CsvWritingTest extends \PHPUnit_Framework_TestCase
{
	public function testCounting()
	{
		$destFilename = '/tmp/apk-iterators.output.csv';

		$csvIterator = CsvFileReader::create(__DIR__ . '/../assets/us-500.csv', true)
			->toCollector(CsvFileWriter::create($destFilename, true, ',', '"'))
			->run()
		;

		$checkFile = fopen($destFilename, 'r');
		$checksum = 0;
		while (false !== fgets($checkFile)) {
			$checksum++;
		}
		fclose($checkFile);
		unlink($destFilename);

		$this->assertEquals(501, $checksum);
	}

}
