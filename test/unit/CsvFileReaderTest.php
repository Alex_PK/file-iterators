<?php

/**
 * This file is part of apk/file-iterators
 *
 * (c) Copyright 2015 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace FileIteratorsTests\Unit;

use Apk\FileIterators\Generator\CsvFileReader;

class CsvFileReaderTest extends \PHPUnit_Framework_TestCase
{
	public function testCounting()
	{
		$csvIterator = CsvFileReader::create(__DIR__ . '/../assets/us-500.csv', true, ',', '"');
		$count = $csvIterator
			->count()
		;

		$this->assertEquals(500, $count);
	}

	/**
	 * @expectedException \RuntimeException
	 */
	public function testNonExistingFile()
	{
		$csvIterator = CsvFileReader::create('unexistant.csv', true, ',', '"');
	}
}
