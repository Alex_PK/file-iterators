<?php

/**
 * This file is part of apk/file-iterators
 *
 * (c) Copyright 2015 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\FileIterators\Consumer;

use Apk\Iterators\Consumer\CnsumerInterface;
use Apk\Iterators\StaticTrait;

class CsvFileWriter implements ConsumerInterface
{
	use StaticTrait;

	protected $destFilename;
	protected $keysOnFirstLine = false;
	protected $delimiter = ',';
	protected $enclosure = '"';

	protected $outputFile = null;
	protected $headerWritten = false;

	public function __construct($destFile, $keysOnFirstLine=false,  $delimiter = ",", $enclosure = "\"")
	{
		if (!is_string($destFile)) {
			throw new \InvalidArgumentException('File name must be a string');
		}
		if (!is_bool($keysOnFirstLine) && !is_array($keysOnFirstLine)) {
			throw new \InvalidArgumentException('keysOnFirstLine must be a boolean or an array of keys');
		}
		if (mb_strlen($delimiter) != 1 || mb_strlen($enclosure) != 1) {
			throw new \InvalidArgumentException('Delimiter and enclosure must be a single character');
		}

		$this->destFilename = $destFile;
		$this->keysOnFirstLine = $keysOnFirstLine;
		$this->delimiter = $delimiter;
		$this->enclosure = $enclosure;
	}

	public function open()
	{
		if (is_null($this->outputFile)) {
			$this->outputFile = fopen($this->destFilename, 'w');
		}
	}

	public function write($item)
	{
		if ( ! $this->headerWritten && $this->keysOnFirstLine) {
			if (is_bool($this->keysOnFirstLine)) {
				fputcsv($this->outputFile, array_keys($item), $this->delimiter, $this->enclosure);

			} else {
				fputcsv($this->outputFile, $this->keysOnFirstLine, $this->delimiter, $this->enclosure);
			}

			$this->headerWritten = true;
		}

		fputcsv($this->outputFile, $item, $this->delimiter, $this->enclosure);
	}

	public function close()
	{
		if (!is_null($this->outputFile)) {
			fclose($this->outputFile);
		}
	}
}
 
