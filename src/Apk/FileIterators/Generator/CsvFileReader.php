<?php

/**
 * This file is part of apk/file-iterators
 *
 * (c) Copyright 2015 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\FileIterators\Generator;

use Apk\Iterators\ConsumerTrait;
use Apk\Iterators\AdaptorTrait;
use Apk\Iterators\StaticTrait;

class CsvFileReader extends \SplFileObject
{
	use StaticTrait;
	use AdaptorTrait;
	use ConsumerTrait;

	protected $keysOnFirstLine = false;
	protected $keys;

	public function __construct($fileName, $keysOnFirstLine=false, $delimiter = ",", $enclosure = "\"", $escape = "\\", $maxLineLen = 65536)
	{
		if (!file_exists($fileName)) {
			throw new \RuntimeException('File does not exist');
		}

		if (ini_get('auto_detect_line_endings') == false) {
			$checkFile = fopen($fileName, 'r');
			$checkChunk = fread($checkFile, $maxLineLen);
			if (strpos($checkChunk, "\r") !== false) {
				throw new \InvalidArgumentException('File has odd line endings. Consider using ini_set("auto_detect_line_endings", true);');
			}
			fclose($checkFile);
		}

		parent::__construct($fileName);
		$this->setCsvControl($delimiter, $enclosure, $escape);
		$this->setMaxLineLen($maxLineLen);
		$this->setFlags(\SplFileObject::READ_CSV | \SplFileObject::READ_AHEAD | \SplFileObject::SKIP_EMPTY | \SplFileObject::DROP_NEW_LINE);

		$this->keysOnFirstLine = (bool)$keysOnFirstLine;
	}

	public function rewind()
	{
		parent::rewind();
		if ($this->keysOnFirstLine) {
			$this->keys = parent::current();
			parent::next();
		}
	}

	public function current()
	{
		$row = parent::current();

		if ($this->keysOnFirstLine) {
			return array_combine($this->keys, $row);
		} else {
			return $row;
		}
	}

	public function getKeys()
	{
		return $this->keys;
	}
}
